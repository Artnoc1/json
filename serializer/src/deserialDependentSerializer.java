public class deserialDependentSerializer extends JSONItems{

    private static int stringIndexCounter = 0;
    private static int objIndexCounter = 0;

    private static boolean objectsLeft() {
        return objIndexCounter < firstPartList.size();
    }

    private static void makeJSONStringForTheObject (int nextObj) {
        int nextNextObj = nextObj + 1;
        objIndexCounter++;
        if (!yesNextObjInPreviousObj(nextNextObj) && secondPartList.get(nextObj).size() > 1) {
            makeJSONStringForArray();
            objIndexCounter--;
            return;
        }
        objIndexCounter--;
        StringBuilder builder = new StringBuilder(serializedStr.get(stringIndexCounter));
        builder.append(firstPartList.get(nextObj));
        builder.append(": ");
        for (int line = 0; line < secondPartList.get(nextObj).size(); line++) {
            builder.append(secondPartList.get(nextObj).get(line));
            builder.append(",\n");
        }
        serializedStr.set(stringIndexCounter, builder.toString());
    }

    private static void closeJSONStringForCurlyObject () {
        StringBuilder finalBuilder = new StringBuilder(serializedStr.get(stringIndexCounter));
        finalBuilder.insert(0, firstPartList.get(objIndexCounter) + ": {\n");
        finalBuilder.setLength(finalBuilder.length() - 2);
        finalBuilder.append("\n},\n");
        serializedStr.set(stringIndexCounter, finalBuilder.toString());
    }

    private static void makeJSONStringForArray () {
        StringBuilder arrayBuilder = new StringBuilder(serializedStr.get(stringIndexCounter));
        arrayBuilder.append(firstPartList.get(objIndexCounter));
        arrayBuilder.append(": [\n");
        for (int member = 0; member < secondPartList.get(objIndexCounter).size(); member++) {
            arrayBuilder.append("      ");
            arrayBuilder.append(secondPartList.get(objIndexCounter).get(member));
            arrayBuilder.append(",\n");
        }
        arrayBuilder.setLength(arrayBuilder.length() - 2);
        arrayBuilder.append("\n],\n");
        serializedStr.set(stringIndexCounter, arrayBuilder.toString());
    }

    private static void nonCurlyMakeJSONString() {
        if (secondPartList.get(objIndexCounter).size() == 1) {
            makeJSONStringForTheObject(objIndexCounter);
        }else {
            makeJSONStringForArray();
        }
    }

    private static boolean yesNextObjInPreviousObj (int nextIndex) {
        if (nextIndex < secondPartList.size()) {
           /* for (int object = 0; object < secondPartList.get(objIndexCounter).size(); object++) {
                if (secondPartList.get(objIndexCounter).get(object).getClass().getName().equals("java.util.Double")) {
                    String replace = secondPartList.get(objIndexCounter).get(object).toString();
                    secondPartList.get(objIndexCounter).set(object, replace);
                } // todo: this is for preventing the double to be shown in scientific notation
            }// todo: problem is solvable using a loop to find the number of digits of the double, and String.format it
            int sampleLength = secondPartList.get(nextIndex).toString().length();
            String sample = secondPartList.get(nextIndex).toString().substring(1, sampleLength - 1);
            sample = sample.replaceAll("\\s+", ""); */
            return /*secondPartList.get(objIndexCounter).toString().contains(sample) &&*/
                    secondPartList.get(objIndexCounter).toString().contains(firstPartList.get(nextIndex));
        }
        return false;
    }

    private static void checkIfNextObjectIsInThePreviousOne () {
        int nextObject = objIndexCounter + 1;
        if (yesNextObjInPreviousObj(nextObject)) {
            serializedStr.add("");
            while (yesNextObjInPreviousObj(nextObject)) {
                makeJSONStringForTheObject(nextObject);
                nextObject++;
            }
            closeJSONStringForCurlyObject();
            objIndexCounter = nextObject - 1;
        }else {
            serializedStr.add("");
            nonCurlyMakeJSONString();
        }
        stringIndexCounter++;
    }

    private static void wrapUP () {
        serializedStr.add(0, "{\n");
        int lastString = serializedStr.size() - 1;
        StringBuilder strBuilder = new StringBuilder(serializedStr.get(lastString));
        strBuilder.setLength(serializedStr.get(lastString).length() - 2);
        serializedStr.set(lastString, strBuilder.toString());
        serializedStr.add("\n}");
    }

    public static void serializer () {
        while (objectsLeft()) {
            checkIfNextObjectIsInThePreviousOne();
            objIndexCounter++;
        }
        wrapUP();
    }
}
