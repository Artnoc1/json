import java.util.Scanner;

public class MainSerializerProgram extends JSONItems {
    public static void main(String[] args) {
        MainSerializerProgram main = new MainSerializerProgram();
        main.begin();
    }

    private void begin () {
        Scanner input = new Scanner(System.in);
        System.out.println("\nDo you want to deserialize first? ** if \"YES\" then enter 'y': ");
        System.out.println("Otherwise (if your data is already deserialized) press Enter to skip...");
        boolean passTheLoop = false;

        while (!passTheLoop) {
            String userInput = input.nextLine();

            if (userInput.equals("y")) {
                startReceivingInput.start();
                goToSerializerNumOne();
                passTheLoop = true;

            }else if (userInput.equals("")) {
                goToSerializerNumTwo();
                passTheLoop = true;

            }else {
                System.out.println("Please only enter 'y' or press Enter to skip.");
            }
        }
        printJSONString();
    }

    private static void goToSerializerNumOne () {
        deserialDependentSerializer.serializer();
    }

    private static void goToSerializerNumTwo () {
        independentSerializer.serializer();
    }

    private static void printJSONString () {
        for (String str : serializedStr) {
            System.out.print(str);
        }
    }
}
