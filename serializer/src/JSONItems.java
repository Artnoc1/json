import java.util.ArrayList;
import java.util.HashMap;

public class JSONItems {
    protected static String JSON;
    protected static HashMap<String, Object> map = new HashMap<>();
    protected static ArrayList<String> firstPartList = new ArrayList<>();
    protected static ArrayList<ArrayList<Object>> secondPartList = new ArrayList<>();
    protected static ArrayList<Object> objectArrayList = new ArrayList<>();
    // The following variables are used in the serializer...
    protected static ArrayList<String> serializedStr =  new ArrayList<>();
}
